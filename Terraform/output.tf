output "instance_ip_addr" {
  value       = aws_instance.app_server.public_ip
  description = "The pablic IP address of the main server instance."
}
