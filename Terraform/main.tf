provider "aws" {
  profile = "default"
  region  = "us-west-1"
}

data "aws_ami" "ubuntu" {
     owners      = ["099720109477"] # Ubuntu
     most_recent = true
     filter {
       name   = "name"
       values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
     }
     filter {
       name   = "virtualization-type"
       values = ["hvm"]
    }
}

resource "aws_instance" "app_server" {
  ami        = data.aws_ami.ubuntu.id
  instance_type   = "t2.micro"
  vpc_security_group_ids = [aws_security_group.app-server-group.id]
  key_name        = "server3"
  tags = {
    Name  = "App-Server"
  }
}

resource "aws_security_group" "app-server-group" {
  name        = "Servers Security Group"
  description = "Security group for accessing traffic to our App"


  dynamic "ingress" {
    for_each = ["80", "22", "81", "8080"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Server SecurityGroup"
  }
}


